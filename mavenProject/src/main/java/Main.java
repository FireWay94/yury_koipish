import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);
    private static Connection connection;
    private static Statement statement;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db");
        statement = connection.createStatement();
        try {
            while (continueUserAction()) ;
        } catch (SQLException e) {
            System.out.println("Database error!");
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    private static boolean continueUserAction() throws SQLException {
        System.out.println("Insert 1 to add new employee, 2 to delete employee, 3 to print EmployeeList, 4 to exit:");
        try {
            switch (Integer.parseInt(sc.nextLine())) {
                case 1:
                    add();
                    break;
                case 2:
                    remove();
                    break;
                case 3:
                    print();
                    break;
                case 4:
                    return false;
            }
        } catch (NumberFormatException e) {
            System.out.println("Check your input!");
        }
        return true;
    }

    private static void add() throws SQLException {
        System.out.println("Name:");
        String firstname = sc.nextLine();
        System.out.println("Lastname:");
        String lastname = sc.nextLine();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Employee(firstname, lastname) VALUES (?, ?)");
        preparedStatement.setString(1, firstname);
        preparedStatement.setString(2, lastname);
        preparedStatement.execute();
        if (preparedStatement != null) {
            preparedStatement.close();
        }
    }

    private static void remove() throws SQLException {
        System.out.println("ID:");
        int id = Integer.parseInt(sc.nextLine());
        statement.executeUpdate("DELETE FROM Employee WHERE id=" + id);
    }

    private static void print() throws SQLException {
        ResultSet resultSet = statement.executeQuery("SELECT * FROM Employee");
        while (resultSet.next()) {
            String id = resultSet.getString("id");
            String firstname = resultSet.getString(2);
            String lastname = resultSet.getString(3);
            System.out.println(id + " " + firstname + " " + lastname);
        }
    }
}