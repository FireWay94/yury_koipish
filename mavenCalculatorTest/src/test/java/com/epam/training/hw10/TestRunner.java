package com.epam.training.hw10;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> files = Arrays.asList("./runAllTests.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}