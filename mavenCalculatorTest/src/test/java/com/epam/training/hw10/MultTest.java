package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTest extends BaseTest {

    @DataProvider
    public Object[][] dataSetLong() {
        return new Object[][]{
                {4L, 3L, 12L},
                {5L, -7L, -35L},
                {-3L, -5L, 15L},
                {0L, 8L, 0L},
                {0L, 0L, 0L},
        };
    }

    @DataProvider
    public Object[][] dataSetDouble() {
        return new Object[][]{
                {3D, 5D, 15D},
                {-2.1D, 2D, -4.1D},
                {-1.3D, -3.5D, 4.55D},
                {0D, 0D, 0D},
        };
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "dataSetLong")
    public void testMultLong(long a, long b, long expected) {
        Assert.assertEquals(calculator.mult(a, b), expected, "Wrong multiplex result for " + a + " and " + b + "!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "dataSetDouble")
    public void testMultDouble(double a, double b, double expected) {
        Assert.assertEquals(calculator.mult(a, b), expected, "Wrong multiplex result for " + a + " and " + b + "!");
    }

    @Test(groups = {"double"}, dataProvider = "dataSetLong")
    public void testMultDoubleWithLong(double a, double b, double expected) {
        Assert.assertEquals(calculator.mult(a, b), expected, "Wrong multiplex result.");
    }
}