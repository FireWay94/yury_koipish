package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DivTest extends BaseTest {

    @DataProvider
    public Object[][] dataSetLong() {
        return new Object[][]{
                {18L, 3L, 6L},
                {-21L, 7L, -3L},
                {-88L, -11L, 8L},
                {0L, -5L, 0L},
        };
    }

    @DataProvider
    public Object[][] dataSetDouble() {
        return new Object[][]{
                {1.8D, 0.3D, 6D},
                {-25.25D, 2.5D, -10.1D},
                {-88D, -11D, 8D},
                {0D, -1.4D, 0D},
        };
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "dataSetLong", priority = 3)
    public void testDivLong(long a, long b, long expected) {
        Assert.assertEquals(calculator.div(a, b), expected, "Wrong division result for " + a + " and " + b + "!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "dataSetDouble", priority = 4)
    public void testDivDouble(double a, double b, double expected) {
        Assert.assertEquals(calculator.div(a, b), expected, "Wrong division result for " + a + " and " + b + "!");
    }

    @Test(groups = {"smoke", "long"}, expectedExceptions = NumberFormatException.class, priority = 1)
    public void testDivLongByZero() {
        calculator.div(1L, 0L);
    }

    @Test(groups = {"smoke", "double"}, expectedExceptions = Exception.class, priority = 2)
    public void testDivDoubleByZero() {
        calculator.div(1D, 0D);
    }
}