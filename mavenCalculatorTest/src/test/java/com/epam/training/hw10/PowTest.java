package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PowTest extends BaseTest {

    @DataProvider
    public Object[][] powDataSet() {
        return new Object[][]{
                {2.5D, 3D},
                {0D, 5D},
                {2.11D, -1.4D},
                {7.5D, -3.2D},
                {-2D, 3D},
                {0D, 0D},
                {14L, 3L},
        };
    }

    @DataProvider
    public Object[][] sqrtDataSet() {
        return new Object[][]{
                {25D},
                {81.81D},
                {1.1D},
                {0D},
                {14L},
        };
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "powDataSet")
    public void testPow(double a, double b) {
        Assert.assertEquals(calculator.pow(a, b), Math.pow(a, b), "Wrong number to a power result!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "sqrtDataSet")
    public void testSqrt(double a) {
        Assert.assertEquals(calculator.sqrt(a), Math.sqrt(a), "Wrong root of number for " + a + "!");
    }

    @Test(groups = {"double"})
    public void testSqrtWithNegative() {
        double a = -25D;
        Assert.assertEquals(calculator.sqrt(a), Math.sqrt(a), "Wrong root of number for " + a + "!");
    }
}