package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TypeOfNumberTest extends BaseTest {

    @DataProvider
    public Object[][] positiveDataSet() {
        return new Object[][]{
                {-1L, false},
                {0L, false},
                {1L, true},
        };
    }

    @DataProvider
    public Object[][] negativeDatSet() {
        return new Object[][]{
                {-1L, true},
                {0L, false},
                {1L, false},
        };
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "positiveDataSet")
    public void testIsPositive(long a, boolean expected) {
        Assert.assertEquals(calculator.isPositive(a), expected, a + " is not positive!");
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "negativeDatSet")
    public void testIsNegative(long a, boolean expected) {
        Assert.assertEquals(calculator.isNegative(a), expected, a + " is not negative!");
    }
}