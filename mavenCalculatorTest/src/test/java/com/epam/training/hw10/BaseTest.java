package com.epam.training.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    protected Calculator calculator = new Calculator();

    @BeforeClass
    public void initializeSuit() {
        System.out.println("Suit start...");
    }

    @AfterClass
    public void endSuit() {
        System.out.println("Suit end.");
    }
}