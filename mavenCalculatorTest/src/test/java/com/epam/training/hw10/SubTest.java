package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SubTest extends BaseTest {

    @DataProvider
    public Object[][] dataSetLong() {
        return new Object[][]{
                {10L, 3L, 7L},
                {7L, -9L, 16L},
                {-35L, 41L, -76L},
                {-150L, -1700L, 1550L},
        };
    }

    @DataProvider
    public Object[][] dataSetDouble() {
        return new Object[][]{
                {10.5D, 3.5D, 7D},
                {7.1D, -9D, 16.1D},
                {-35D, 41.25D, -76.25D},
                {-150D, -1700D, 1550D},
        };
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "dataSetLong")
    public void testSubLong(long a, long b, long expected) {
        Assert.assertEquals(calculator.sub(a, b), expected, "Wrong difference result for " + a + " and " + b + "!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "dataSetDouble")
    public void testSubDouble(double a, double b, double expected) {
        Assert.assertEquals(calculator.sub(a, b), expected, "Wrong difference result for " + a + " and " + b + "!");
    }
}