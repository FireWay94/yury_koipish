package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTest extends BaseTest {
    @DataProvider
    public Object[][] dataSetLong() {
        return new Object[][]{
                {10L, 3L, 13L},
                {7L, -9L, -2L},
                {-35L, 41L, 6L},
                {-150L, -1700L, -1850L},
        };
    }

    @DataProvider
    public Object[][] dataSetDouble() {
        return new Object[][]{
                {10.5D, 3.5D, 14D},
                {7.1D, -9D, -1.9D},
                {-35D, 41.25D, 6.25D},
                {-150D, -1700D, -1850D},
        };
    }

    @Test(groups = {"smoke", "long"}, dataProvider = "dataSetLong")
    public void testSumLong(long a, long b, long expected) {
        Assert.assertEquals(calculator.sum(a, b), expected, "Wrong sum result for " + a + " and " + b + "!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "dataSetDouble")
    public void testSumDouble(double a, double b, double expected) {
        Assert.assertEquals(calculator.sum(a, b), expected, "Wrong sum result for " + a + " and " + b + "!");
    }
}