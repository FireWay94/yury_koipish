package com.epam.training.hw10;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TrigonometryTest extends BaseTest {

    @DataProvider
    public Object[][] values() {
        return new Object[][]{
                {-2.5D},
                {-1D},
                {0D},
                {1D},
                {2.5D},
        };
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "values")
    public void testSin(double a) {
        Assert.assertEquals(calculator.sin(a), Math.sin(a), "Sinus for " + a + " is not correct!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "values")
    public void testCos(double a) {
        Assert.assertEquals(calculator.cos(a), Math.cos(a), "Cosinus for " + a + " is not correct!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "values")
    public void testTg(double a) {
        Assert.assertEquals(calculator.tg(a), Math.tan(a), "Tangens for " + a + " is not correct!");
    }

    @Test(groups = {"smoke", "double"}, dataProvider = "values")
    public void testCtg(double a) {
        Assert.assertEquals(calculator.ctg(a), 1.0 / Math.tan(a), "Cotangens for " + a + " is not correct!");
    }
}