package com.epam.training.hw4.Task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SortingContextTest {
    private static SortingContext context = new SortingContext();

    @Test
    public void testExecuteWithBubble() {
        context.sortStrategy = new BubbleSort();
        int[] array = {21, 2, -5, 40, -9, 1, 0, 1, -90, 11, -1, 71, -17, 12};
        int[] arrayExpected = {21, 2, -5, 40, -9, 1, 0, 1, -90, 11, -1, 71, -17, 12};
        context.execute(array);
        Arrays.sort(arrayExpected);
        Assert.assertArrayEquals(arrayExpected, array);
    }

    @Test
    public void testExecuteWithSelection() {
        context.sortStrategy = new SelectionSort();
        int[] array = {21, 2, -5, 40, -9, 1, 0, 1, -90, 11, -1, 71, -17, 12};
        int[] arrayExpected = {21, 2, -5, 40, -9, 1, 0, 1, -90, 11, -1, 71, -17, 12};
        context.execute(array);
        Arrays.sort(arrayExpected);
        Assert.assertArrayEquals(arrayExpected, array);
    }
}