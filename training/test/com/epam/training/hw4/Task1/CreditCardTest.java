package com.epam.training.hw4.Task1;

import org.junit.Assert;
import org.junit.Test;

public class CreditCardTest {
    CreditCard creditCard = new CreditCard("Jenny", 1000);

    @Test
    public void testDecreaseBalance() {
        creditCard.decreaseBalance(400);
        Assert.assertEquals(600, creditCard.getBalance());
    }

    @Test
    public void testDecreaseBalanceNegative() {
        creditCard.decreaseBalance(1500);
        Assert.assertEquals(-500, creditCard.getBalance());
    }

    @Test
    public void testIncreaseBalanse() {
        creditCard.increaseBalance(500);
        Assert.assertEquals(1500, creditCard.getBalance());
    }

    @Test
    public void testConvertBalance() {
        Assert.assertEquals(2390, creditCard.convertBalance(2.39));
    }
}