package com.epam.training.hw4.Task1;

import org.junit.Assert;
import org.junit.Test;

public class DebitCardTest {
    DebitCard debitCard;

    {
        try {
            debitCard = new DebitCard("Tony", 1000);
        } catch (NegativeBalanceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDecreaseBalance() throws NegativeBalanceException {
        debitCard.decreaseBalance(400);
        Assert.assertEquals(600, debitCard.getBalance());
    }

    @Test(expected = NegativeBalanceException.class)
    public void testDecreaseBalanceNegative() throws NegativeBalanceException {
        debitCard.decreaseBalance(1500);
    }

    @Test
    public void testIncreaseBalanse() {
        debitCard.increaseBalance(500);
        Assert.assertEquals(1500, debitCard.getBalance());
    }

    @Test
    public void testConvertBalance() {
        Assert.assertEquals(2390, debitCard.convertBalance(2.39));
    }
}