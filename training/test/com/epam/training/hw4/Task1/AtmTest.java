package com.epam.training.hw4.Task1;

import org.junit.Assert;
import org.junit.Test;

public class AtmTest {
    Atm creditCard = new Atm(new CreditCard("Jenny", 1000.5));
    Atm debitCard;

    {
        try {
            debitCard = new Atm(new DebitCard("Tony", 1000));
        } catch (NegativeBalanceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIncreaseBalanceCreditCard() {
        creditCard.increaseCardBalance(500.5);
        Assert.assertEquals(1501, creditCard.getCardBalance());
    }

    @Test
    public void testIncreaseBalanceDebitCard() {
        debitCard.increaseCardBalance(500);
        Assert.assertEquals(1500, debitCard.getCardBalance());
    }

    @Test
    public void testDecreaseBalanseCreditCard() {
        creditCard.decreaseCardBalance(500);
        Assert.assertEquals(500.5, creditCard.getCardBalance());
    }

    @Test
    public void testDecreaseBalanseCreditCardNegative() {
        creditCard.decreaseCardBalance(1500);
        Assert.assertEquals(-499.5, creditCard.getCardBalance());
    }

    @Test
    public void testDecreaseBalanseDebitCard() {
        debitCard.decreaseCardBalance(500);
        Assert.assertEquals(500, debitCard.getCardBalance());
    }

    @Test
    public void testDecreaseBalanseDebitCardNegative() {
        try {
            debitCard.decreaseCardBalance(1500);
        } catch (NegativeArraySizeException e) {
            Assert.assertEquals(100, debitCard.getCardBalance());
        }
    }
}