package com.epam.training.hw5;

import org.junit.Assert;
import org.junit.Test;

import static com.epam.training.hw5.Reader.readTextFromFile;
import static com.epam.training.hw5.TextDelimiter.getSeparatedText;

public class TextDelimiterTest {

    @Test
    public void testSeparateTextEmptyString() {
        Assert.assertEquals("", getSeparatedText(""));
    }

    @Test
    public void testSeparateTextOneLetter() {
        Assert.assertEquals("V: v \n", getSeparatedText("v???  !!!"));
    }

    @Test
    public void testSeparateText() {
        Assert.assertEquals("A: a at \n" +
                "H: hillside \n" +
                "L: lapping \n" +
                "O: on once \n" +
                "S: spring \n" +
                "T: time \n" +
                "U: upon \n" +
                "W: was wolf \n", getSeparatedText("Once upon a time a Wolf was lapping at a spring on a hillside."));
    }

    @Test
    public void testTextFromFile() {
        Assert.assertEquals("1: 1864 1867 1921 1970 \n" +
                "A: all already an and are as available \n" +
                "B: become began but \n" +
                "C: continues \n" +
                "D: discontinued \n" +
                "E: editions extra \n" +
                "I: in initiated issued \n" +
                "L: listed \n" +
                "N: not numbered \n" +
                "O: obtainable occasionally of only or original \n" +
                "P: part present printed publications \n" +
                "R: readily \n" +
                "S: satisfactory series started subsequently suitable supplementary \n" +
                "T: texts the this to \n" +
                "V: volumes \n" +
                "W: was were \n", getSeparatedText(readTextFromFile("src/com/epam/training/hw5/text.txt")));
    }
}