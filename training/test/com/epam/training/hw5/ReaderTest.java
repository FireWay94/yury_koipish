package com.epam.training.hw5;

import org.junit.Assert;
import org.junit.Test;

import static com.epam.training.hw5.Reader.readTextFromFile;

public class ReaderTest {

    @Test
    public void testReadTextFromFile() {
        Assert.assertEquals("Hello world!", readTextFromFile("src/com/epam/training/hw5/forReaderTest.txt"));
    }

    @Test
    public void testFileNotExist() {
        Assert.assertEquals("", readTextFromFile("src/com/epam/training/hw5/reader.txt"));
    }
}