package com.epam.training.hw3;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    private Card cardTony = new Card("Tony", 3500.1);

    @Test
    public void testIncreaseBalance() {
        Card cardBred = new Card("Bred");
        cardBred.increaseBalance(500);
        Assert.assertEquals(500, cardBred.getBalance());
    }

    @Test
    public void testDecreaseBalance() {

        cardTony.decreaseBalance(501.1);
        Assert.assertEquals(2999, cardTony.getBalance());
    }

    @Test
    public void testConvertBalance() {
        Assert.assertEquals(8435.241, cardTony.convertBalance(2.41));
    }
}