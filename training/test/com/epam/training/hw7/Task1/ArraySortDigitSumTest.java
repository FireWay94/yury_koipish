package com.epam.training.hw7.Task1;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class ArraySortDigitSumTest {

    @Test
    public void testSortArrayByDigitSum() {
        Integer[] array = new Integer[]{199, 9, 100, 550, 21};
        Integer[] expectedArray = new Integer[]{100, 21, 9, 550, 199};
        Arrays.sort(array, new ArraySortDigitSum());
        Assert.assertArrayEquals(expectedArray, array);
    }

    @Test
    public void testSortArrayByDigitSumWithNeg() {
        Integer[] array = new Integer[]{-199, 9, -100, 550, -21};
        Integer[] expectedArray = new Integer[]{-100, -21, 9, 550, -199};
        Arrays.sort(array, new ArraySortDigitSum());
        Assert.assertArrayEquals(expectedArray, array);
    }
}