package com.epam.training.hw7.Task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static com.epam.training.hw7.Task2.MultiplicityOperations.*;

public class MultiplicityOperationsTest {
    private static HashSet<Integer> multiplicity;
    private static HashSet<Integer> multiplicity1;
    private static HashSet<Integer> multiplicity2;

    @Before
    public void initialize() {
        multiplicity = new HashSet<>();
        multiplicity1 = new HashSet<>();
        multiplicity2 = new HashSet<>();
        multiplicity1.add(1);
        multiplicity1.add(2);
        multiplicity2.add(2);
        multiplicity2.add(3);
    }

    @Test
    public void testGetUnion() {
        multiplicity.add(1);
        multiplicity.add(2);
        multiplicity.add(3);
        Assert.assertEquals(multiplicity, getUnion(multiplicity1, multiplicity2));
    }

    @Test
    public void testGetIntersection() {
        multiplicity.add(2);
        Assert.assertEquals(multiplicity, getIntersection(multiplicity1, multiplicity2));
    }

    @Test
    public void testGetRelativeCompliment() {
        multiplicity.add(1);
        Assert.assertEquals(multiplicity, getRelativeCompliment(multiplicity1, multiplicity2));
    }

    @Test
    public void testGetSymmetricDifference() {
        multiplicity.add(1);
        multiplicity.add(3);
        Assert.assertEquals(multiplicity, getSymmetricDifference(multiplicity1, multiplicity2));
    }
}