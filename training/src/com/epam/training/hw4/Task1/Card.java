package com.epam.training.hw4.Task1;

public abstract class Card {
    String ownerName;
    double balance;

    public Card(String ownerName) {
        this.ownerName = ownerName;
        balance = 0;
    }

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void increaseBalance(double increase) {
        balance += increase;
    }

    abstract void decreaseBalance(double decrease) throws NegativeBalanceException;

    public double convertBalance(double course) {
        return balance * course;
    }
}