package com.epam.training.hw4.Task1;

public class NegativeBalanceException extends Exception {
    public NegativeBalanceException() {
        super("Error! Insufficient funds to complete the transaction.");
    }

    public NegativeBalanceException(String message) {
        super(message);
    }
}