package com.epam.training.hw4.Task1;

public class DebitCard extends Card {
    public DebitCard(String ownerName) {
        super(ownerName);
    }

    public DebitCard(String ownerName, double balance) throws NegativeBalanceException {
        super(ownerName);
        if (balance < 0) {
            throw new NegativeBalanceException("DebitCard balance can not be negative. Balance is 0.");
        } else increaseBalance(balance);
    }

    @Override
    public void decreaseBalance(double decrease) throws NegativeBalanceException {
        if (decrease > getBalance()) {
            throw new NegativeBalanceException();
        } else balance -= decrease;
    }
}