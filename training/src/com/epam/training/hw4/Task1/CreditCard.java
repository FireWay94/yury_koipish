package com.epam.training.hw4.Task1;

public class CreditCard extends Card {
    public CreditCard(String ownerName) {
        super(ownerName);
    }

    public CreditCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    @Override
    public void decreaseBalance(double decrease) {
        balance -= decrease;
    }
}