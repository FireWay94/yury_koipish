package com.epam.training.hw4.Task1;

public class Atm {
    private Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public double getCardBalance() {
        return card.getBalance();
    }

    public void increaseCardBalance(double increase) {
        card.increaseBalance(increase);
    }

    public void decreaseCardBalance(double decrease) {
        try {
            card.decreaseBalance(decrease);
        } catch (NegativeBalanceException e) {
            e.printStackTrace();
        }
    }
}