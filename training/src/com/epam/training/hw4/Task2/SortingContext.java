package com.epam.training.hw4.Task2;

public class SortingContext {
    public Sorter sortStrategy;

    public SortingContext() {
        if (Math.random() > 0.5) {
            this.sortStrategy = new BubbleSort();
        } else {
            this.sortStrategy = new SelectionSort();
        }
    }

    public void execute(int[] array) {
        sortStrategy.sort(array);
    }
}