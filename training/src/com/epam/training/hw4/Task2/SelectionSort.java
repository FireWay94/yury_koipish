package com.epam.training.hw4.Task2;

public class SelectionSort extends Sorter {
    public void sort(int[] array) {
        System.out.println("Selection");
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int iMin = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    iMin = j;
                }
            }
            if (i != iMin) {
                int tmp = array[i];
                array[i] = array[iMin];
                array[iMin] = tmp;
            }
        }
    }
}