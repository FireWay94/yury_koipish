package com.epam.training.hw4.Task2;

public abstract class Sorter {
    abstract void sort(int[] array);
}