package com.epam.training.hw2;

public class Sequence {
    public static int intValue = 13;
    int class_parameter;

    public Sequence() {
        class_parameter = 1;
        System.out.println("Constructor");
    }

    {
        System.out.println("Class_parameter not initialize " + class_parameter);
        System.out.println("Initialize block 1");
    }

    {
        System.out.println("Initialize block 2");
    }

    static {
        System.out.println("intValue already initialized " + intValue);
        System.out.println("Static block 1");
    }

    static {
        System.out.println("Static block 2");
    }

    public static void main(String[] args) {
        Sequence first;
        Sequence second = new Sequence();
        first = second;
        System.out.println("Class_parameter initialize " + first.class_parameter);
    }
}
