package com.epam.training.hw2;

public class Recursion {
    public static int count = 2;

    public static void main(String[] args) {
        System.out.println("Last Fibbonachi number for int is " + findCountFibbonachiMembersInt(1, 1));
        count = 2;
        System.out.println("Last Fibbonachi number for long is " + findCountFibbonachiMembersLong(1, 1));
    }

    public static int findCountFibbonachiMembersInt(int previous, int last) {
        long previousLong = previous;
        long lastLong = last;
        int resultInt = previous + last;
        long resultLong = previousLong + lastLong;
        if (resultInt == resultLong) {
            count++;
            findCountFibbonachiMembersInt(last, resultInt);
        }
        return count;
    }

    public static int findCountFibbonachiMembersLong(long previousLong, long lastLong) {
        long resultLong = previousLong + lastLong;
        if (previousLong < Long.MAX_VALUE - lastLong) {
            count++;
            findCountFibbonachiMembersLong(lastLong, resultLong);
        }
        return count;
    }
}
