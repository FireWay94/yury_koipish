package com.epam.training.hw2;

public class Initializer {
    private static boolean defaultBoolean;
    private static char defaultChar;
    private static byte defaultByte;
    private static short defaultShort;
    private static int defaultInt;
    private static long defaultLong;
    private static float defaultFloat;
    private static double defaultDouble;
    private static Boolean defaultBooleanRef;
    private static Character defaultCharRef;
    private static Byte defaultByteRef;
    private static Short defaultShortRef;
    private static Integer defaultIntRef;
    private static Long defaultLongRef;
    private static Float defaultFloatRef;
    private static Double defaultDoubleRef;
    private static int[] defaultIntArray;
    private static Integer[] defaultIntegerArray;

    public static void main(String[] args) {
        System.out.println("boolean: " + defaultBoolean);
        System.out.println("char: " + defaultChar);
        System.out.println("byte: " + defaultByte);
        System.out.println("short: " + defaultShort);
        System.out.println("int: " + defaultInt);
        System.out.println("long: " + defaultLong);
        System.out.println("float: " + defaultFloat);
        System.out.println("double: " + defaultDouble);

        System.out.println("Boolean: " + defaultBooleanRef);
        System.out.println("Character: " + defaultCharRef);
        System.out.println("Byte: " + defaultByteRef);
        System.out.println("Short: " + defaultShortRef);
        System.out.println("Integer: " + defaultIntRef);
        System.out.println("Long: " + defaultLongRef);
        System.out.println("Float: " + defaultFloatRef);
        System.out.println("Double: " + defaultDoubleRef);
        System.out.println("Array int: " + defaultIntArray);
        System.out.println("Array Integer: " + defaultIntegerArray);
    }
}
