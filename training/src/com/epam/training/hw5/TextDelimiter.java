package com.epam.training.hw5;

import java.util.Arrays;

public class TextDelimiter {
    private static String[] wordsArray;
    private static String separatedText;

    public static String getSeparatedText(String text) {
        wordsArray = null;
        separatedText = "";
        if (checkText(text)) {
            Arrays.sort(wordsArray);
            prepareResultString();
        }
        return separatedText;
    }

    private static void separateText(String text) {
        wordsArray = text.toLowerCase().split("[\\ ,;:.!?\\-()#$@\\s]+");
    }

    private static boolean checkText(String text) {
        while (text.length() != 0) {
            if (!Character.isLetterOrDigit(text.charAt(0))) {
                text = text.substring(1);
            } else {
                break;
            }
        }
        if (text.equals("")) {
            System.out.println("String is empty or not contains letters or digits.");
            return false;
        }
        separateText(text);
        if (wordsArray == null || wordsArray.length == 0) {
            System.out.println("String is not contains letters or digits.");
            return false;
        }
        return true;
    }

    private static void prepareResultString() {
        String firstLetter = wordsArray[0].substring(0, 1);
        for (int i = 0; i < wordsArray.length; i++) {
            i = addWordsWithLetter(firstLetter, i);
            if (!(i >= wordsArray.length)) {
                firstLetter = wordsArray[i].substring(0, 1);
                i--;
            }
        }
    }

    private static int addWordsWithLetter(String firstLetter, int i) {
        separatedText += firstLetter.toUpperCase() + ": ";
        String match = "";
        while (i < wordsArray.length && wordsArray[i].matches("^" + firstLetter + "\\w*$")) {
            if (!match.contains(wordsArray[i] + " ")) {
                match += wordsArray[i] + " ";
            }
            i++;
        }
        separatedText += match + "\n";
        return i;
    }
}