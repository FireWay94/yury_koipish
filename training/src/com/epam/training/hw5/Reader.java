package com.epam.training.hw5;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Reader {
    public static String readTextFromFile(String path) {
        String text = "";
        try {
            FileInputStream fstream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                text += strLine;
            }
            fstream.close();

        } catch (IOException e) {
            System.out.println("Error!");
        }
        return text;
    }
}