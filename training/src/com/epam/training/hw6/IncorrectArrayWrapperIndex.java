package com.epam.training.hw6;

public class IncorrectArrayWrapperIndex extends RuntimeException {
    public IncorrectArrayWrapperIndex() {
        super("Index was outside the bounds of the array.");
    }
}