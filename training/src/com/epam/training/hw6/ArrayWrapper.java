package com.epam.training.hw6;

public class ArrayWrapper<T> {
    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        checkIndex(index);
        return array[index - 1];
    }

    public boolean replace(int index, T value) {
        checkIndex(index);
        if (!isValidValue(index, value)) {
            return false;
        }
        array[index - 1] = value;
        return true;
    }

    private void checkIndex(int index) {
        if (index < 1 || index > array.length) {
            throw new IncorrectArrayWrapperIndex();
        }
    }

    private boolean isValidValue(int index, T value) {
        if (value instanceof Integer && (Integer) value <= (Integer) array[index - 1]) {
            System.out.println("You can only replace number to higher value.");
            return false;
        }
        if (value instanceof String && value.toString().length() <= array[index - 1].toString().length()) {
            System.out.println("You can replace the string only with a string of greater length.");
            return false;
        }
        return true;
    }
}