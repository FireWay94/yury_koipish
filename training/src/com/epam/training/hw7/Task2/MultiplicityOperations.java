package com.epam.training.hw7.Task2;

import java.util.HashSet;

public class MultiplicityOperations {
    public static <T> HashSet getUnion(HashSet<T> multiplicity1, HashSet<T> multiplicity2) {
        multiplicity1.addAll(multiplicity2);
        return multiplicity1;
    }

    public static <T> HashSet getIntersection(HashSet<T> multiplicity1, HashSet<T> multiplicity2) {
        multiplicity1.retainAll(multiplicity2);
        return multiplicity1;
    }

    public static <T> HashSet getRelativeCompliment(HashSet<T> multiplicity1, HashSet<T> multiplicity2) {
        multiplicity2.retainAll(multiplicity1);
        multiplicity1.removeAll(multiplicity2);
        return multiplicity1;
    }

    public static <T> HashSet getSymmetricDifference(HashSet<T> multiplicity1, HashSet<T> multiplicity2) {
        HashSet<T> multiplicity = new HashSet<>();
        multiplicity.addAll(multiplicity1);
        multiplicity.addAll(multiplicity2);
        multiplicity1.retainAll(multiplicity2);
        multiplicity.removeAll(multiplicity1);
        return multiplicity;
    }
}