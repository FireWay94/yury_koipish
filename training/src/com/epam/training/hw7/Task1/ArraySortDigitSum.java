package com.epam.training.hw7.Task1;

import java.util.Comparator;

public class ArraySortDigitSum implements Comparator<Integer> {
    private static int getNumberDigitSum(int number) {
        if (number < 0) {
            number *= -1;
        }
        int sum = 0;
        while (number >= 10) {
            sum += number % 10;
            number /= 10;
        }
        sum += number;
        return sum;
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        if (getNumberDigitSum(o1) > getNumberDigitSum(o2)) {
            return 1;
        }
        if (getNumberDigitSum(o1) < getNumberDigitSum(o2)) {
            return -1;
        }
        return 0;
    }
}