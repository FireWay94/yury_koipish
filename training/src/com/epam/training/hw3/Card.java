package com.epam.training.hw3;

public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName) {
        this.ownerName = ownerName;
        balance = 0;
    }

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void increaseBalance(double increase) {
        balance += increase;
    }

    public void decreaseBalance(double decrease) {
        balance -= decrease;
    }

    public double convertBalance(double course) {
        return balance * course;
    }
}


