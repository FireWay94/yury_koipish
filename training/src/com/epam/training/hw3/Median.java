package com.epam.training.hw3;

import java.util.Arrays;

public class Median {
    public static float median(int[] array) {
        if (array.length == 0) {
            return 0;
        }
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            return (array[array.length / 2] + array[array.length / 2 - 1]) / 2F;
        } else return array[array.length / 2];
    }

    public static double median(double[] array) {
        if (array.length == 0) {
            return 0;
        }
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            return (array[array.length / 2] + array[array.length / 2 - 1]) / 2F;
        } else return array[array.length / 2];
    }
}
