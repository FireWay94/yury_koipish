package com.epam.training.hw1;

public class FormulaCalculation {

    private static int a;
    private static int p;
    private static double m1;
    private static double m2;

    public static void main(String[] args) {
        if (validateParameters(args)) {
            System.out.println("G = " + calculateG(a, p, m1, m2));
        }
    }

    public static boolean validateParameters(String[] args) {
        try {
            a = Integer.parseInt(args[0]);
            p = Integer.parseInt(args[1]);
            m1 = Double.parseDouble(args[2]);
            m2 = Double.parseDouble(args[3]);
        } catch (NumberFormatException e) {
            System.out.println("Error! Incorrect input or type overflow!\nProgram takes four arguments separated by a space. For example: 2 5 8.2 7.1");
            return false;
        }
        if (p == 0 || (m1 + m2) == 0) {
            System.out.println("Error! Divided by zero!");
            return false;
        }
        return true;
    }

    public static double calculateG(int a, int p, double m1, double m2) {
        return 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2));
    }
}