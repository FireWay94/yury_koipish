package com.epam.training.hw1;

import java.util.Scanner;

public class LoopAlgorithms {

    private static int count;
    private static int previous = 1;
    private static int last = 1;
    private static int result;
    private static int fact = 1;
    public static int[] parameters;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter 3 parameters: ");
        validateParameters(sc.nextLine());
        if (parameters != null) {
            calculate(parameters[0], parameters[1], parameters[2]);
        }
    }

    public static void validateParameters(String parametersStr) {
        String[] argsArray = parametersStr.split("[ ]");
        parameters = new int[argsArray.length];
        try {
            for (int i = 0; i < argsArray.length; i++) {
                parameters[i] = Integer.parseInt(argsArray[i]);
            }
        } catch (NumberFormatException e) {
            System.out.print("Incorrect input or type overflow!");
            return;
        }
        if (parameters[0] != 1 && parameters[0] != 2) {
            System.out.println("Incorrect first parameter!");
            parameters = null;
            return;
        }
        if (parameters[1] < 1 || parameters[1] > 3) {
            System.out.println("Incorrect second parameter!");
            parameters = null;
            return;
        }
        if (parameters[2] < 0) {
            System.out.println("Incorrect third parameter!");
            parameters = null;
            return;
        }
    }

    public static void calculate(int algorithmID, int loopType, int n) {
        if (algorithmID == 1) {
            calculateFibo(loopType, n);
        }
        if (algorithmID == 2) {
            calculateFactorial(loopType, n);
        }
    }

    public static void calculateFibo(int loopType, int n) {
        if (n == 0) {
            System.out.print(0);
        } else if (n == 1) {
            System.out.print(1);
        } else if (n == 2) {
            System.out.print("1 1");
        } else {
            System.out.print("1 1 ");
            count = 2;
            switch (loopType) {
                case 1:
                    calculateFiboWithWhile(n);
                    break;
                case 2:
                    calculateFiboWithDoWhile(n);
                    break;
                case 3:
                    calculateFiboWithFor(n);
                    break;
            }
        }
    }

    public static void calculateFactorial(int loopType, int n) {
        count = 1;
        switch (loopType) {
            case 1:
                System.out.println("Factorial by While cycle " + calculateFactorialWithWhile(n));
                break;
            case 2:
                System.out.println("Factorial by Do-While cycle " + calculateFactorialWithDoWhile(n));
                break;
            case 3:
                System.out.println("Factorial by For cycle " + calculateFactorialWithFor(n));
                break;
        }
    }

    public static void calculateFiboWithWhile(int n) {
        while (n > count++) {
            result = previous + last;
            previous = last;
            last = result;
            System.out.print(result + " ");
        }
    }

    public static void calculateFiboWithDoWhile(int n) {
        do {
            result = previous + last;
            previous = last;
            last = result;
            System.out.print(result + " ");
        } while (n > ++count);
    }

    public static void calculateFiboWithFor(int n) {
        for (count = 2; count < n; count++) {
            result = previous + last;
            previous = last;
            last = result;
            System.out.print(result + " ");
        }
    }

    public static int calculateFactorialWithWhile(int n) {
        while (n > count) {
            fact *= ++count;
        }
        return fact;
    }

    public static int calculateFactorialWithDoWhile(int n) {
        do {
            fact *= count++;
        }
        while (n >= count);
        return fact;
    }

    public static int calculateFactorialWithFor(int n) {
        for (count = 1; count < n; count++, fact *= count) ;
        return fact;
    }
}