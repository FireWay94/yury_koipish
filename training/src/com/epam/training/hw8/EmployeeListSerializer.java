package com.epam.training.hw8;

import java.io.*;

public class EmployeeListSerializer implements Serializable {
    private static FileInputStream fis;
    private static ObjectInputStream ois;
    private static FileOutputStream fos;
    private static ObjectOutputStream oos;

    public static boolean serializeEmployee(EmployeeList employees) throws IOException {
        try {
            fos = new FileOutputStream("dbEmployee.ser");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(employees);

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            oos.close();
            fos.close();
        }
        return true;
    }

    public static EmployeeList deserializeEmployee() throws IOException {
        EmployeeList employees = new EmployeeList();
        if (!(new File("dbEmployee.ser").exists())) {
            return employees;
        }
        try {
            fis = new FileInputStream("dbEmployee.ser");
            ois = new ObjectInputStream(fis);
            employees = (EmployeeList) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            ois.close();
            fis.close();
        }
        return employees;
    }
}