package com.epam.training.hw8;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeList implements Serializable {
    private List<Employee> employees;

    public EmployeeList() {
        employees = new ArrayList<>();
    }

    public void add(String firstname, String lastname) {
        int id = 0;
        for (Employee employee : employees) {
            if (employee.getId() > id) {
                id = employee.getId();
            }
        }
        id++;
        employees.add(new Employee(id, firstname, lastname));
    }

    public boolean delete(int id) {
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                employees.remove(employee);
                return true;
            }
        }
        return false;
    }

    public String print() {
        String employeeListStr = "";
        for (Employee employee : employees) {
            employeeListStr += employee.toString() + "\n";
        }
        return employeeListStr;
    }

    public int count() {
        return employees.size();
    }
}