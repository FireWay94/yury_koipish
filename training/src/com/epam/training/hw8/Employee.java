package com.epam.training.hw8;

import java.io.Serializable;

public class Employee implements Serializable {
    private int id;
    private String firstname;
    private String lastname;

    public Employee(int id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "ID: " + id + "\nName: " + firstname + "\nLastname: " + lastname;
    }
}