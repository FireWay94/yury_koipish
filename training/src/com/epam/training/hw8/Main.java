package com.epam.training.hw8;

import java.io.IOException;
import java.util.Scanner;

import static com.epam.training.hw8.EmployeeListSerializer.*;

public class Main {
    private static EmployeeList list = new EmployeeList();
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            list = deserializeEmployee();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (list.count() == 0) {
            System.out.println("File with db not found. New file was created.");
        } else {
            System.out.println("Load completed!");
        }
        while (continueUserAction()) ;
    }

    private static boolean continueUserAction() {
        try {
            System.out.println("Insert 1 to add new employee, 2 to delete employee, 3 to print EmployeeList, 4 to exit:");
            switch (Integer.parseInt(sc.nextLine())) {
                case 1:
                    add();
                    break;
                case 2:
                    remove();
                    break;
                case 3:
                    print();
                    break;
                case 4:
                    save();
                    return false;
            }
        } catch (NumberFormatException e) {
            System.out.println("Check your input!");
        }
        return true;
    }

    private static void add() {
        System.out.println("Name:");
        String firstname = sc.nextLine();
        System.out.println("Lastname:");
        String lastname = sc.nextLine();
        list.add(firstname, lastname);
    }

    private static void remove() {
        System.out.println("ID:");
        int id = Integer.parseInt(sc.nextLine());
        if (list.delete(id)) {
            System.out.println("Successfully!");
        } else {
            System.out.println("Employee not found!");
        }
    }

    private static void print() {
        System.out.println(list.print());
    }

    private static void save() {
        try {
            serializeEmployee(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}