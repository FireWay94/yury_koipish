package com.epam.at.hw11;

import org.openqa.selenium.By;

public class Locators {
    public static final By ENTER_BUTTON = new By.ByCssSelector("a[data-statlog*='logout.domik.login']");
    public static final By LOGIN_FIELD = new By.ByName("login");
    public static final By PASSWORD_FIELD = new By.ByXPath("//input[@type='password']");
    public static final By LOGO = new By.ById("recipient-1");
    public static final By WRITE_LETTER_BUTTON = new By.ByCssSelector("a.js-main-action-compose");
    public static final By TEXTAREA = new By.ByXPath("//div[@role='textbox']");
    public static final By SEND_TO = new By.ByXPath("//div/div[@name='to']");
    public static final By SUBJECT_FIELD = new By.ByCssSelector("div.mail-Compose-Field-Input>input[type='text']");
    public static final By CLOSE_LETTER = new By.ByXPath("//div[@title='Закрыть']");
    public static final By SAVE = new By.ByXPath("//span[contains(text(),'Сохранить и перейти')]");
    public static final By DRAFTS = new By.ByXPath("//a[@data-fid='6']");
    public static final By DRAFT_ADDRESSEE = new By.ByXPath("//span[@class='mail-MessageSnippet-FromText']");
    public static final By DRAFT_SUBJECT = new By.ByXPath("//input[@class='mail-Compose-Field-Input-Controller js-compose-field js-editor-tabfocus-prev']");
    public static final By DRAFT_TEXTAREA = new By.ByXPath("//div[@role='textbox']/div");
    public static final By SEND = new By.ByXPath("//button[@role='button']");
    public static final By SENT = new By.ByXPath("//a[@data-fid='4']");
    public static final By SENDED_TO = new By.ByXPath("//span[@class='mail-MessageSnippet-FromText']");
    public static final By EXIT = new By.ByClassName("b-mail-dropdown__item__content");
}