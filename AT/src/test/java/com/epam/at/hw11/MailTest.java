package com.epam.at.hw11;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MailTest extends BaseTest {
    public static final String ADDRESSE = "yurik_racer94@mail.ru";
    public static final String SUBJECT = "Test";
    public static final String MESSAGE = "Automatisation test";
    public static final String URL = "https://yandex.by/";
    public static final String LOGIN = "trainingepam";
    public static final String PASSWORD = "123654789qwe";

    @BeforeMethod
    public void logIn() {
        driver.get(URL);
        driver.findElement(Locators.ENTER_BUTTON).click();
        driver.findElement(Locators.LOGIN_FIELD).sendKeys(LOGIN, Keys.ENTER);
        waitForElementVisibility(Locators.PASSWORD_FIELD);
        driver.findElement(Locators.PASSWORD_FIELD).sendKeys(PASSWORD, Keys.ENTER);
    }

    @Test
    public void testAutorization() {
        Assert.assertTrue(driver.findElement(Locators.LOGO).isDisplayed());
    }

    @Test
    public void testCreateDraft() {
        waitForElementVisibility(Locators.WRITE_LETTER_BUTTON);
        driver.findElement(Locators.WRITE_LETTER_BUTTON).click();
        waitForElementVisibility(Locators.TEXTAREA);
        driver.findElement(Locators.TEXTAREA).click();
        driver.findElement(Locators.TEXTAREA).sendKeys(MESSAGE);
        driver.findElement(Locators.SEND_TO).click();
        driver.findElement(Locators.SEND_TO).sendKeys(ADDRESSE);
        waitForElementVisibility(Locators.SUBJECT_FIELD);
        driver.findElement(Locators.SUBJECT_FIELD).click();
        driver.findElement(Locators.SUBJECT_FIELD).sendKeys(SUBJECT);
        driver.findElement(Locators.CLOSE_LETTER).click();
        waitForElementVisibility(Locators.SAVE);
        driver.findElement(Locators.SAVE).click();
        driver.findElement(Locators.DRAFTS).click();
        driver.findElement(Locators.DRAFTS).sendKeys(Keys.F9);
        String addressee = driver.findElement(Locators.DRAFT_ADDRESSEE).getAttribute("title");
        driver.findElement(Locators.DRAFT_ADDRESSEE).click();
        String subject = driver.findElement(Locators.DRAFT_SUBJECT).getAttribute("value");
        String text = driver.findElement(Locators.DRAFT_TEXTAREA).getText();
        Assert.assertTrue(addressee.equals(ADDRESSE) && subject.equals(SUBJECT) && text.equals(MESSAGE));
    }

    @Test
    public void testSendEmail() {
        waitForElementVisibility(Locators.WRITE_LETTER_BUTTON);
        driver.findElement(Locators.WRITE_LETTER_BUTTON).click();
        waitForElementVisibility(Locators.TEXTAREA);
        driver.findElement(Locators.TEXTAREA).click();
        driver.findElement(Locators.TEXTAREA).sendKeys(MESSAGE);
        driver.findElement(Locators.SEND_TO).click();
        driver.findElement(Locators.SEND_TO).sendKeys(ADDRESSE);
        waitForElementVisibility(Locators.SUBJECT_FIELD);
        driver.findElement(Locators.SUBJECT_FIELD).click();
        driver.findElement(Locators.SUBJECT_FIELD).sendKeys(SUBJECT);
        driver.findElement(Locators.CLOSE_LETTER).click();
        waitForElementVisibility(Locators.SAVE);
        driver.findElement(Locators.SAVE).click();
        driver.findElement(Locators.DRAFTS).click();
        driver.findElement(Locators.DRAFTS).sendKeys(Keys.F9);
        String addressee = driver.findElement(Locators.DRAFT_ADDRESSEE).getAttribute("title");
        driver.findElement(Locators.DRAFT_ADDRESSEE).click();
        String subject = driver.findElement(Locators.DRAFT_SUBJECT).getAttribute("value");
        String text = driver.findElement(Locators.DRAFT_TEXTAREA).getText();
        Assert.assertTrue(addressee.equals(ADDRESSE) && subject.equals(SUBJECT) && text.equals(MESSAGE));
        driver.findElement(Locators.SEND).click();
        driver.findElement(Locators.SENT).click();
        driver.findElement(Locators.SENT).sendKeys(Keys.F9);
        addressee = driver.findElement(Locators.SENDED_TO).getAttribute("title");
        Assert.assertEquals(addressee, ADDRESSE);
    }

    @Test
    public void testLogOut() {
        driver.findElement(Locators.LOGO).click();
        driver.findElements(Locators.EXIT).get(3).click();
        Assert.assertTrue(driver.findElement(Locators.ENTER_BUTTON).isDisplayed());
    }
}