package com.epam.at.hw15.impl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeCreator implements BrowserCreator {
    private static final String CHROME_PATH = "src/test/resources/drivers/chromedriver.exe";

    public WebDriver create() {
        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        return driver;
    }
}