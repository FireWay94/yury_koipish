package com.epam.at.hw15.businessObjects;

import java.util.Random;

public class MailFactory {
    public static Mail generateMailWithAddresse(String addresse) {
        String subject = String.valueOf((new Random()).nextInt());
        String message = System.currentTimeMillis() + "";
        return new Mail(addresse, subject, message);
    }
}