package com.epam.at.hw15.services;

import com.epam.at.hw15.businessObjects.Mail;
import com.epam.at.hw15.businessObjects.User;
import com.epam.at.hw15.pages.LoginPage;
import com.epam.at.hw15.pages.MailLettersPage;

public class MailOptions {

    public static MailLettersPage logIn(LoginPage loginPage, User user) {
        return loginPage.clickEnterButton()
                        .fillLogin(user.getLogin())
                        .fillPassword(user.getPassword())
                        .clickSubmit();
    }

    public static MailLettersPage writeLetter(MailLettersPage mailLettersPage, Mail mail) {
        mailLettersPage.clickWriteLetterButton()
                       .fillInTextarea(mail.getMessage())
                       .fillInSubjectField(mail.getSubject())
                       .fillInAddresseField(mail.getAddresse())
                       .closeLetter();
        return mailLettersPage;
    }

    public static boolean checkLetterInDrafts(MailLettersPage mailLettersPage, Mail mail) {
        return mailLettersPage.openDrafts().checkLetterInDrafts(mail);
    }

    public static MailLettersPage sendLetterFromDrafts(MailLettersPage mailLettersPage, Mail mail) {
        mailLettersPage.openDrafts()
                       .openDraft(mail)
                       .clickSendLetterButton();
        return mailLettersPage;
    }

    public static boolean checkLetterInSent(MailLettersPage mailLettersPage, Mail mail) {
        return mailLettersPage.openSent().checkLetterInSent(mail);
    }

    public static LoginPage logOut(MailLettersPage mailLettersPage) {
        return mailLettersPage.clickExitButton();
    }

    public static boolean isLoadLoginPage(LoginPage loginPage) {
        return loginPage.isLoad();
    }

    public static boolean isLoadMailPage(MailLettersPage mailLettersPage) {
        return mailLettersPage.isLoad();
    }
}