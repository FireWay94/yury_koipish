package com.epam.at.hw15.impl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxCreator implements BrowserCreator {
    private static final String FIREFOX_PATH = "src/test/resources/drivers/geckodriver.exe";

    public WebDriver create() {
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver", FIREFOX_PATH);
        driver = new FirefoxDriver();
        return driver;
    }
}