package com.epam.at.hw15.tests;

import com.epam.at.hw15.impl.DriverSingleton;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    @BeforeMethod
    public static void setUp() {
        DriverSingleton.getInstance();
    }

    @AfterMethod
    public static void tearDown() {
        DriverSingleton.quitDriver();
    }
}