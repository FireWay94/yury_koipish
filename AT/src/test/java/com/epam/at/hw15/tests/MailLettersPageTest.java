package com.epam.at.hw15.tests;

import com.epam.at.hw15.businessObjects.Mail;
import com.epam.at.hw15.businessObjects.MailFactory;
import com.epam.at.hw15.businessObjects.User;
import com.epam.at.hw15.businessObjects.UserFactory;
import com.epam.at.hw15.pages.LoginPage;
import com.epam.at.hw15.pages.MailLettersPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.epam.at.hw15.services.MailOptions.*;


public class MailLettersPageTest extends BaseTest {
    private static final User user = UserFactory.getUser();
    private static final Mail mail = MailFactory.generateMailWithAddresse("yurik_racer94@mail.ru");
    private MailLettersPage mailLettersPage;

    @BeforeMethod
    public void authorize() {
        mailLettersPage = logIn((new LoginPage()).open(), user);
    }

    @Test
    public void testAuthorization() {
        Assert.assertTrue(isLoadMailPage(mailLettersPage));
    }

    @Test
    public void testCreateDraft() {
        writeLetter(mailLettersPage, mail);
        Assert.assertTrue(checkLetterInDrafts(mailLettersPage, mail));
    }

    @Test
    public void testSendEmail() {
        writeLetter(mailLettersPage, mail);
        sendLetterFromDrafts(mailLettersPage, mail);
        Assert.assertTrue(checkLetterInSent(mailLettersPage, mail));
    }

    @Test
    public void testLogOut() {
        Assert.assertTrue(isLoadLoginPage(logOut(mailLettersPage)));
    }
}