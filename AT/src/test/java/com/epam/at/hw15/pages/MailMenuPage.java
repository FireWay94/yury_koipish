package com.epam.at.hw15.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class MailMenuPage extends Page {
    public static final By LOGO = new By.ById("recipient-1");
    public static final By WRITE_LETTER_BUTTON = new By.ByCssSelector("a.js-main-action-compose");
    public static final By DRAFTS = new By.ByXPath("//a[@href='#draft']");
    public static final By SENT = new By.ByXPath("//a[@data-fid='4']");
    public static final By EXIT = new By.ByXPath("//div[@class='_nb-popup-content']/div/*[8]/a");

    public MailMenuPage() {
        super();
    }

    public LoginPage clickExitButton() {
        waitForElementVisibility(LOGO);
        getDriver().findElement(LOGO).click();
        waitForElementVisibility(EXIT);
        getDriver().findElement(EXIT).click();
        return new LoginPage();
    }

    public MailLettersPage clickWriteLetterButton() {
        waitForElementVisibility(WRITE_LETTER_BUTTON);
        getDriver().findElement(WRITE_LETTER_BUTTON).click();
        return new MailLettersPage();
    }

    public MailLettersPage openDrafts() {
        getDriver().findElement(DRAFTS).click();
        getDriver().findElement(DRAFTS).sendKeys(Keys.F9);
        return new MailLettersPage();
    }

    public MailLettersPage openSent() {
        getDriver().findElement(SENT).click();
        getDriver().findElement(SENT).sendKeys(Keys.F9);
        return new MailLettersPage();
    }

    public boolean isLoad() {
        return getDriver().findElement(LOGO).isDisplayed();
    }
}