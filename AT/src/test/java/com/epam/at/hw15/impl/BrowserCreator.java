package com.epam.at.hw15.impl;

import org.openqa.selenium.WebDriver;

public interface BrowserCreator {
    WebDriver create();
}