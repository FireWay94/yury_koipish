package com.epam.at.hw15.pages;

import com.epam.at.hw15.impl.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class Page {

    public Page() {
    }

    public WebDriver getDriver() {
        return DriverSingleton.getInstance();
    }

    public abstract boolean isLoad();

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(DriverSingleton.getInstance(), 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}