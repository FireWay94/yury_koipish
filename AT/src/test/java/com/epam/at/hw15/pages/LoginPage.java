package com.epam.at.hw15.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class LoginPage extends Page {
    private static final String BASE_URL = "https://yandex.by/";
    public static final By ENTER_BUTTON = new By.ByCssSelector("a[data-statlog*='logout.domik.login']");
    public static final By LOGIN_FIELD = new By.ByName("login");
    public static final By PASSWORD_FIELD = new By.ByXPath("//input[@type='password']");

    public LoginPage() {
        super();
    }

    public LoginPage open() {
        getDriver().get(BASE_URL);
        return new LoginPage();
    }

    public LoginPage clickEnterButton() {
        waitForElementVisibility(ENTER_BUTTON);
        getDriver().findElement(ENTER_BUTTON).click();
        return this;
    }

    public LoginPage fillLogin(String login) {
        getDriver().findElement(LOGIN_FIELD).sendKeys(login, Keys.ENTER);
        return this;
    }

    public LoginPage fillPassword(String password) {
        waitForElementVisibility(PASSWORD_FIELD);
        getDriver().findElement(PASSWORD_FIELD).sendKeys(password);
        return this;
    }

    public MailLettersPage clickSubmit() {
        getDriver().findElement(PASSWORD_FIELD).sendKeys(Keys.ENTER);
        return new MailLettersPage();
    }

    public boolean isLoad() {
        return getDriver().findElement(ENTER_BUTTON).isDisplayed();
    }
}