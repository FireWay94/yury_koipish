package com.epam.at.hw15.businessObjects;

public class UserFactory {
    private static final String LOGIN = "trainingepam";
    private static final String PASSWORD = "123654789qwe";

    public static User getUser() {
        return new User(LOGIN, PASSWORD);
    }
}