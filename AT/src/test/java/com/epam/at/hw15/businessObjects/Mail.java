package com.epam.at.hw15.businessObjects;

public class Mail {
    private String addresse;
    private String subject;
    private String message;

    public Mail(String addresse, String subject, String message) {
        this.addresse = addresse;
        this.subject = subject;
        this.message = message;
    }

    public String getAddresse() {
        return addresse;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public boolean equals(Mail obj) {
        if (addresse.equals(obj.getAddresse()) && subject.equals(obj.getSubject()) && message.equals(obj.getMessage())) {
            return true;
        }
        return false;
    }
}