package com.epam.at.hw15.pages;

import com.epam.at.hw15.businessObjects.Mail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MailLettersPage extends MailMenuPage {
    public static final By TEXTAREA = new By.ByXPath("//div[@role='textbox']");
    public static final By SEND_TO = new By.ByXPath("//div/div[@name='to']");
    public static final By SUBJECT_FIELD = new By.ByCssSelector("div.mail-Compose-Field-Input>input[type='text']");
    public static final By CLOSE_LETTER = new By.ByXPath("//div[@title='Закрыть']");
    public static final By SAVE = new By.ByXPath("//span[contains(text(),'Сохранить и перейти')]");
    public static final By DRAFT_ADDRESSEE = new By.ByXPath("//span[@class='mail-MessageSnippet-FromText']");
    public static final By DRAFT_SUBJECT = new By.ByXPath("//input[@class='mail-Compose-Field-Input-Controller js-compose-field js-editor-tabfocus-prev']");
    public static final By DRAFT_TEXTAREA = new By.ByXPath("//div[@role='textbox']/div");
    public static final By SEND = new By.ByXPath("//button[@role='button']");
    public static final By SENDED_TO = new By.ByXPath("//span[@class='mail-MessageSnippet-FromText']");

    public MailLettersPage() {
        super();
    }

    public MailLettersPage fillInTextarea(String message) {
        fillInField(TEXTAREA, message);
        return this;
    }

    public MailLettersPage fillInAddresseField(String addressee) {
        fillInField(SEND_TO, addressee);
        return this;
    }

    public MailLettersPage fillInSubjectField(String subject) {
        fillInField(SUBJECT_FIELD, subject);
        return this;
    }

    private void fillInField(By locator, String text) {
        waitForElementVisibility(locator);
        getDriver().findElement(locator).click();
        getDriver().findElement(locator).sendKeys(text);
    }

    public MailLettersPage closeLetter() {
        getDriver().findElement(CLOSE_LETTER).click();
        waitForElementVisibility(SAVE);
        getDriver().findElement(SAVE).click();
        return this;
    }

    public MailLettersPage openDraft(Mail mail) {
        if (checkLetterInDrafts(mail)) {
            getDriver().findElement(DRAFT_ADDRESSEE).click();
        }
        return this;
    }

    public boolean checkLetterInDrafts(Mail mail) {
        waitForElementVisibility(DRAFT_ADDRESSEE);
        List<WebElement> drafts = getDriver().findElements(DRAFT_ADDRESSEE);
        for (WebElement draft : drafts) {
            if (checkEmail(draft, mail)) {
                return true;
            }
        }
        return false;
    }

    public MailLettersPage clickSendLetterButton() {
        getDriver().findElement(SEND).click();
        return this;
    }

    public boolean checkLetterInSent(Mail mail) {
        waitForElementVisibility(SENDED_TO);
        if (getDriver().findElement(SENDED_TO).getAttribute("title").equals(mail.getAddresse())) {
            return true;
        }
        return false;
    }

    private boolean checkEmail(WebElement letter, Mail mail) {
        waitForElementVisibility(DRAFT_ADDRESSEE);
        String addressee = getDriver().findElement(DRAFT_ADDRESSEE).getAttribute("title");
        letter.click();
        waitForElementVisibility(DRAFT_SUBJECT);
        String subject = getDriver().findElement(DRAFT_SUBJECT).getAttribute("value");
        String message = getDriver().findElement(DRAFT_TEXTAREA).getText();
        if (new Mail(addressee, subject, message).equals(mail)) {
            getDriver().navigate().back();
            return true;
        }
        getDriver().navigate().back();
        return false;
    }
}